## Participant Systems



- **Electronic Health Records (EHR) Systems:** Platforms for recording and tracking patient treatment and progress.
- **Telehealth Services:** Offering remote consultations and monitoring.
- **Caregivers:** Family members or professional caregivers assisting patients.

## Information Flow

- **Input:** Treatment plans, medication schedules, patient adherence data.
- **Process:** Patients following treatment plans, regular monitoring by healthcare providers, updates in EHR systems.
- **Output:** Health progress reports, adjustments to treatment plans, refill reminders from pharmacies.