### Input

- **Structured Data:** Includes patient health records, diagnostic results, insurance claims, and personal health data from apps.
- **Example:** Maria’s health data from MySugr and records from ELGA.

### Process

1. **Data Collection:**
    - Patients input data into apps; healthcare providers record data into EHRs.
2. **Data Sharing:**
    - EHRs like ELGA facilitate data sharing among providers, while apps sync with personal health records.
3. **Data Analysis:**
    - Data stewards analyze and convert data for clinical decisions and public health insights.
4. **Monitoring and Adjustments:**
    - Continuous monitoring of patient adherence and health outcomes through telehealth services and apps.

### Output

- **Diagnoses and Treatments:** Based on analyzed data, leading to tailored treatment plans.
- **Example:** Adjustments to Maria’s diabetes management plan based on her app data and follow-up consultations.

**Systems Involved:**

- **Healthcare Service Consumption:** ELGA, diagnostic labs, insurance companies (ÖGK).
- **Following a Treatment Regime:** Pharmacies, telehealth platforms, health apps.

### Impact on Patient Engagement and Self-Help

- **Enhanced Self-Management:** Patients like Maria actively engage with their health through apps, informed by data from healthcare providers.
- **Improved Health Outcomes:** Coordinated care and continuous feedback ensure effective treatment adherence and timely adjustments.

By mapping these systems and processes, we see a comprehensive integration of data flow that enhances healthcare delivery and patient engagement in Austria.