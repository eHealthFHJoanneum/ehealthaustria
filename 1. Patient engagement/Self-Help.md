## Participant Systems

1. **Patients:** Individuals seeking information and solutions for their health concerns

    - **Role:** Seek information and solutions for health-related issues
    - **Usage:** Use online resources, apps, and engage with healthcare providers for self-care
    
2.  **Web Platforms:** Online resources where patients search for information (e.g., health forums, informational websites)
    
    - **Examples:**
        - **NetDoktor.at:** Provides health information, articles, and a symptom checker. It allows users to search for symptoms, read articles, and access health tools. 
        - **Gesundheit.gv.at:** Official health portal offering comprehensive health information, services, and directories. Offers detailed medical information, directories for healthcare services and eHealth services. 
        
    - **Goals:** Educate patients, provide reliable health information, facilitate symptom checking

3. **Healthcare Apps:** Mobile applications that offer self-diagnosis tools, health monitoring, and wellness tips
    
    - **Examples:**
        - **MySugr:** Diabetes management app that helps track blood sugar levels, carb intake, and insulin doses. User input (e.g. blood sugar data,...) users receive analytics and can access educational resources. 
    
        - **Runtastic:** Fitness app that tracks physical activity, provides exercise plans, and monitors health metrics. Tracks workouts, offers personalized, exercise plans, and connects with other health apps. 

    - **Goals:** Assist in health management, provide personalized health data tracking, and promote wellness.
    
4. **Pharmacies:** Local or online pharmacies providing over-the-counter medication and health advice
    
    - **Examples:**
        - **Shop-Apotheke.at:** Offers a wide range of medications and health products with online consultations. Provides medication details, allows online ordering, and offerd pharmacist consultations. 
    
    - **Goals:** Provide access to medications, offer health advice, facilitate online ordering of health products.

5. **Support Groups:** Both online and offline self-help groups where patients can share experiences and advice
    
    - **Examples in Austria:**
        - **Selbsthilfe Österreich:** Network of self-help groups across various health issues. Lists support groups, facilitates group meetings, and provides resources. 
        - **Caritas Selbsthilfegruppen:** Provides support groups for various conditions and social issues. Organizes group meetings, offers counseling, and shares informational resources. 
        
    - **Goals:** Provide peer support, share experiences, and offer emotional and practical support.
    
6. **Healthcare Providers:** General Practitioners (GPs) offering guidance through patient portals or telehealth services
    
    - **Examples in Austria:**
        - **Patientenportal.at:** Allows patients to access their health records and communicate with healthcare providers. Patients can view medical records, book appointments, and receive medical advice. 
        - [[ELGA (Elektronische Gesundheitsakte)]] Austria's national electronic health record system
        - **Telehealth Services (e.g., Ordinationen via telemedicine):** Offers remote consultations and follow-ups. Provide e-prescriptions and offer follow-up care.  
    
    - **Goals:** Facilitate easy access to healthcare, improve communication between patients and providers, and offer remote healthcare options.

## Information Flow

### Input

- **Patient Symptoms:** Information about the patient's current health issues
- **Health Queries:** Specific questions about health conditions, symptoms, or treatments
- **Medical History:** Accessed through patient portals, containing past medical records and treatments
- **Lifestyle Information:** Data related to diet, exercise, and daily habits

### Process

1. **Researching Symptoms:**
    
    - **Patients** use web platforms like NetDoktor.at to input symptoms into a symptom checker, read articles, and gather information on possible conditions.
    - **Example:** A patient experiencing headaches might use NetDoktor.at to check symptoms, read about potential causes, and consider lifestyle changes or when to see a doctor.
    
2. **Consulting Apps:**
    
    - **Patients** use healthcare apps like MySugr to monitor specific health metrics or manage chronic conditions.
    - **Example:** A diabetic patient uses MySugr to log blood sugar levels, receive insulin dosage recommendations, and track dietary intake.
    
3. **Participating in Support Groups:**
    
    - **Patients** engage with online or offline self-help groups through platforms like Selbsthilfe Österreich.
    - **Example:** A cancer patient joins a support group to share experiences, receive emotional support, and exchange information about treatment options.

**Output:**

- **Self-Care Strategies:** Recommendations for lifestyle changes, home remedies, and preventive measures.
    - **Example:** A patient with mild anxiety learns relaxation techniques and dietary changes through a health-app like "Rootd".

- **OTC Medication:** Advice on over-the-counter medications that can be purchased online or at local pharmacies.
    - **Example:** A patient with a cold finds information on suitable OTC medications on Apotheken.de and orders them online.

- **Appointment Booking:** Scheduling appointments with healthcare providers for further medical advice.
    - **Example:** After researching symptoms, a patient books an appointment with their GP through Patientenportal.at for a thorough examination.

### Concrete Examples and Detailed Processes

**Researching Symptoms:**

- **Example:** Anna, a 30-year-old teacher in Vienna, experiences persistent fatigue. She visits NetDoktor.at, inputs her symptoms, and reads about possible causes like anemia or thyroid issues. She follows links to related articles and decides to monitor her diet and sleep patterns.

**Consulting Apps:**

- **Example:** Thomas, a 45-year-old diabetic in Graz, uses MySugr to log his daily blood sugar readings. The app analyzes his data, suggesting adjustments to his insulin dosage and providing tips on managing his condition. He also connects MySugr with his fitness app, Runtastic, to get a holistic view of his health.

**Participating in Support Groups:**

- **Example:** Petra, a breast cancer survivor in Salzburg, joins a local self-help group through Selbsthilfe Österreich. She attends weekly meetings where members share their experiences, discuss coping strategies, and receive support from healthcare professionals invited by the group.

### Impact on Information Flow Mapping

**Input:**

- Structured through digital platforms and apps, input data includes detailed symptom descriptions, medical history from EHRs, and lifestyle data from fitness apps.
- **Example:** Thomas’ MySugr app collects his blood sugar levels, dietary intake, and insulin doses, integrating this information with his medical history accessed via Patientenportal.at.

**Process:**

- Enhanced by digital tools, the process involves interactive symptom checkers, data analytics from apps, and community support from self-help groups.
- **Example:** Anna’s fatigue-related queries on NetDoktor.at lead her to articles on potential dietary deficiencies, while Runtastic helps her track and improve her sleep patterns.

**Output:**

- Outputs are more personalized and actionable due to integrated systems, providing tailored self-care strategies, precise OTC medication recommendations, and seamless appointment scheduling.
- **Example:** Petra receives personalized advice on managing post-treatment side effects from her support group and books follow-up appointments via telehealth services.

**Further Steps:**

- **Integration with National Health Systems:** Platforms like Gesundheit.gv.at and Patientenportal.at or [[ELGA (Elektronische Gesundheitsakte)]] integrate patient data with national health records, ensuring continuity of care.
- **Feedback Loops:** Patient feedback collected through apps and support groups can be analyzed to improve service delivery and health outcomes.
- **Education and Awareness:** Continuous updates on web platforms and apps educate patients about preventive care, emerging health issues, and available health services in Austria.

By providing concrete examples and detailed descriptions, we see how the participant systems in Austria work together to facilitate effective patient engagement and self-help, ultimately enhancing health outcomes and empowering patients in their healthcare journey.



### Data Provider

**Examples:** Apps like Runtastic (for fitness tracking), MySugr (for diabetes management), where patients input personal health data.


### Data Custodian

**Examples:** HealthVault (a personal health record platform) where patients store and manage their health data.


### Data Steward

**Examples:** Public health agencies analyzing aggregated data from health apps to identify trends and provide guidelines.


### Data Consumer

**Examples:** Patients using app-generated reports to make lifestyle adjustments.


### Health Information Foundation

**Examples:** Standardized data formats used in fitness apps to ensure compatibility with other health platforms.


### Public Information Foundation

**Examples:** Integration of health data with civil registries to enhance public health monitoring.


### Information Policy Controller

**Examples:** Auditing the use of health apps for compliance with privacy regulations.


### Information Policy Owner

**Examples:** Ministry of Health defining regulations for the use of health apps and patient data protection.

