## Participant Systems

1. **Patients:** Individuals requiring medical services.

	- **Role:** Individuals seeking medical services for diagnosis, treatment, and consultation.
	- **Usage:** Utilize healthcare facilities, insurance services, and diagnostic labs for medical care.

2. **Healthcare Providers:** GPs, specialists, hospitals, and clinics providing medical services.

	- **Examples:**
	    - **General Practitioners (GPs):** Local doctors providing primary care. Diagnose common conditions, refer to specialists, provide preventive care.
	    - **Specialists:** Dermatologists, cardiologists, etc., offering specialized medical services. Diagnose and treat specific health conditions, perform surgeries, etc.
	    - **Hospitals and Clinics:** Facilities such as AKH Vienna, offering comprehensive healthcare services. Provide emergency care, inpatient services, surgeries, and specialized treatments.

	- **Goals:** Deliver medical care, diagnose health issues, provide treatments and follow-up care.

3. **Insurance Companies:** Organizations managing patient claims and coverage.

	- **Examples:**
	    - **ÖGK (Österreichische Gesundheitskasse):** Main health insurance provider. Provides coverage for a wide range of medical services, reimburses healthcare providers.
	    - **Private insurers:** Uniqa, Wiener Städtische. Offer additional coverage options, faster access to specialists, private hospital stays.
	
	- **Goals:** Manage patient claims, ensure coverage for medical services, provide financial protection against health expenses.

4. **Diagnostic Labs:** Facilities conducting tests and providing results

	- **Examples:**
	    - **Labors.at:** Network of diagnostic laboratories. Conducts blood tests, imaging, and other diagnostic services.
	    - **Private Labs:** Biogena, Futurelab. Offers specialized tests, fast results.
	
	- **Goals:** Provide accurate diagnostic results to support medical diagnoses and treatment plans.

5. **Government Health Agencies:** Bodies regulating healthcare standards and monitoring public health

	- **Examples in Austria:**
	    - **Ministry of Health:** Regulates healthcare standards and policies. Develops healthcare regulations, oversees healthcare services.
	    - [[Austrian Agency for Health and Food Safety (AGES)]] Monitors public health and safety. Conducts health surveillance, provides health information to the public.
	
	- **Goals:** Ensure high healthcare standards, monitor public health, regulate healthcare providers.



## Information Flow

1. **Input:** Patient health records, insurance details, diagnostic requirements

	- **Patient Health Records:** Detailed medical histories, previous diagnoses, treatments, and ongoing conditions.
	- **Insurance Details:** Coverage plans, patient eligibility, and claim history.
	- **Diagnostic Requirements:** Specific tests needed for diagnosis, such as blood tests, imaging, and biopsies.


2. **Process:** Booking appointments, conducting tests, processing insurance claims, and medical consultations.

	- **Booking Appointments:**
	    
	    - **Systems Involved:** Patientenportal.at for online booking, phone or in-person scheduling at clinics.
	    - **Example:** Maria uses Patientenportal.at to book an appointment with her GP after experiencing persistent stomach pain.
	
	- **Conducting Tests:**
	    
	    - **Systems Involved:** Diagnostic labs like Labors.at.
	    - **Example:** Maria's GP orders a series of tests including blood work and an ultrasound, which are conducted at a local lab.
	
	- **Processing Insurance Claims:**
	    
	    - **Systems Involved:** ÖGK and private insurance platforms.
	    - **Example:** The lab sends the test bill to ÖGK, which processes the claim and reimburses the lab.
	
	- **Medical Consultations:**
	    
	    - **Systems Involved:** GPs, specialists, hospitals.
	    - **Example:** Maria consults with her GP, who refers her to a gastroenterologist for further evaluation.

3. **Output:** Medical diagnoses, treatment plans, and follow-up appointments.

	- **Medical Diagnoses:** Results from tests and consultations leading to a diagnosis.
	    - **Example:** The gastroenterologist diagnoses Maria with gastritis and prescribes a treatment plan.
	
	- **Treatment Plans:** Prescribed medications, lifestyle changes, and follow-up tests or consultations.
	    - **Example:** Maria's treatment plan includes medication, dietary changes, and a follow-up appointment in a month.
	
	- **Follow-Up Appointments:** Scheduling future visits to monitor the patient's progress.
	    - **Example:** Maria books a follow-up appointment through Patientenportal.at to assess her response to the treatment.


## Data Provider

**Definition:**
- Entities that capture the data and where it originates, such as healthcare providers, hospital information systems, or even home users using health apps.

**Examples:** Hospitals (e.g., AKH Vienna), GPs, and specialists who enter patient data into hospital information systems and electronic health records (EHRs).

### Data Custodian

**Definition:**
- Providers of platform services or databases that facilitate data sharing, such as national/regional EHR systems, messaging hubs, or claims registries.

**Examples:** ELGA (Austria's national EHR system) that stores and shares patient records among healthcare providers.


### Data Steward

**Definition:**
- Entities that define, analyze, and convert data into decision-making forms, ensuring data processing is suitable for use, such as public health agencies or clinical decision support systems.

**Examples:** Clinical decision support systems integrated with ELGA, helping doctors make informed decisions based on patient data.

### Data Consumer

**Definition:**
- Users of the data for decision-making, such as ministries, clinicians, or hospital management.

**Examples:** Clinicians using patient records from ELGA to diagnose and treat conditions.

### Health Information Foundation

**Definition:**
- Maintainers of healthcare-specific standards, registries, and terminologies crucial for information flow, such as ICD-10 or HL7.

**Examples:** Use of ICD-10 codes in ELGA for consistent medical records.

### Public Information Foundation

**Definition:**
- Maintainers of public information standards, such as civil or population registries, digital signatures, or messaging platforms.

**Examples:** Use of digital signatures for secure and authenticated health record access.

### Information Policy Controller

**Definition:**
- Entities that coordinate, monitor, and audit the operation of the information flow, ensuring efficiency, such as healthcare quality agencies.

**Examples:** Healthcare quality agencies monitoring the efficiency of data sharing in ELGA.

### Information Policy Owner

**Definition:**
- Definers and evaluators of policies that govern the information flow, such as ministries or healthcare councils.

**Examples:** Ministry of Health setting policies for EHR use and interoperability.
