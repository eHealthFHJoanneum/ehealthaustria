
#### Data Provider - Who captures the data? 

Physicians and Hospitals capture health related data through the patient health record.

Pharmaceutical companies capture all information related to their clinical study in their clinical protocol.

Health data is also captured by Federal Ministries (e.g. Ministry of Health), the States (Länder), national health institutes, epidemiologists, data scientists, different universitys and environmental surveillance systems. 

The data that is usually captured is disease prevalence data, health survey data and environmental health data.

#### Data Custodian - Who provides the platform services? 

- [[ELGA (Elektronische Gesundheitsakte)]]: Austria's national electronic health record system.
- Austrian Epidemiological Surveillance System: Tracks infectious disease data.
- Prescription registries: Monitors prescribed medications.


#### Data Steward - Who defines, analyzes, and converts the data?

- [[Austrian Agency for Health and Food Safety (AGES)]] and other statistical agencies: Analyzing public health data and providing insights for decision-making.
- Public health institutes: Conduct epidemiological research.
- Clinical Decision Support Systems: Used by healthcare providers to inform treatment decisions.
<<<<<<< HEAD
<<<<<<< HEAD
- Pharmaceutical companies: Conduct post-market surveillance to assess the effects of interventions, such as new drugs and vaccines.
<<<<<<< HEAD


>>>>>>> origin/main
=======
- Pharmaceutical companies: Conduct post-market surveillance to assess the effects of interventions, such as new drugs and vaccines.
>>>>>>> origin/main
>>>>>>> 
=======
- Pharmaceutical companies: Conduct post-market surveillance to assess the effects of interventions, such as new drugs and vaccines. 
>>>>>>> origin/main 
>>>>>>> 
